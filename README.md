# Personal Organizer

## System requirements

  1. Install Ruby (version: 2.x)
  2. Install Ruby on Rails (version 5.1.x)
  3. install PostgreSQL

## Clone the repository

  1. From a terminal, change to the directory where you want to clone the repository
  2. Paste the command and hit enter: 
  git clone https://bitbucket.org/clafrance/personal_organizer.git

## System Deployment

  1. From a terminal, change to the repository directory: /personal_organizer
  2. Enter the command: bundle install, this will install the dependencies
  3. Enter the command: rails db:create, this will create the development and test databases
  4. Enter the command: rails db:migrate, this will create tables in the development databases

## Tests

  1. The system is tested using Rspec and Capybara
  2. The Intgration tests are under directory /tests. To run the tests, enter the command from the terminal: rails test
  3. The model tests and helper tests are under directory /spec. To run the tests, enter the command from the terminal: bundle exec rspec

