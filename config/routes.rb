Rails.application.routes.draw do

	resources :subtasks

  resources :tasks do
  	member do
  		get :mark_complete
  	end
  end

  devise_for :users
  root "home#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
