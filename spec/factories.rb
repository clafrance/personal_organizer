FactoryBot.define do

  # factory :user do
  #   first_name "Mary"
  #   last_name "Jean"
  #   email "email@gmail.com"
  #   password "password"
  #   user
  # end

  factory :task do
    body "task_body"
    is_private true
    completed true
    user_id 1
  end

  factory :user do
    first_name "Mary"
    last_name "Jean"
    email "email@gmail.com"
    password "password"
    id 1
  end
end