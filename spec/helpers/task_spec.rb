require 'rails_helper'


RSpec.describe :task, type: :helper do

	it "returns true if current user created the task" do
		user = build(:user)
		task = user.tasks.build(body: "task", completed: true)
		expect(helper.item_owner? user, task).to eq(true)
	end

	# it "returns true if current user created the task" do
	# 	user1 = build(:user)
	# 	user2 = build(:user)
	# 	task1 = user1.tasks.build(body: "task", completed: true)
	# 	task2 = user2.tasks.build(body: "task", completed: true)
	# 	# expect(helper.completed).to eq(true)
	# end

end