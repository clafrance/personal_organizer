require 'rails_helper'


RSpec.describe :task, type: :model do

	it "is completed" do
		user = build(:user)
		task = user.tasks.build(body: "task", completed: true)
		expect(task.completed).to eq(true)
	end

	it "returns display_completed as (completed)" do
		user = build(:user)
		task = user.tasks.build(body: "task", completed: true)
		expect(task.display_completed).to eq("(Completed)")
	end

	it "returns status as Completed: Yes" do
		user = build(:user)
		task = user.tasks.build(body: "task", completed: true)
		expect(task.status).to eq("Completed: Yes")
	end

	it "is not completed" do
		user = build(:user)
		task = user.tasks.build(body: "task3", completed: nil)
		expect(task.completed).to eq(nil)
	end

	it "returns status as Completed: No" do
		user = build(:user)
		task = user.tasks.build(body: "task4", completed: nil)
		expect(task.status).to eq("Completed: No")
	end

	it "returns private as Private" do
		user = build(:user)
		task = user.tasks.build(body: "task4", is_private: true)
		expect(task.private).to eq("Private")
	end

	it "returns private as Public" do
		user = build(:user)
		task = user.tasks.build(body: "task4")
		expect(task.private).to eq("Public")
	end

	it "returns empty user_name" do
		user = build(:user)
		task = user.tasks.build(body: "task4", is_private: true)
		expect(task.user_name).to eq("")
	end

	it "returns user name as Mary Jean" do
		user = build(:user)
		task = user.tasks.build(body: "task4")
		expect(task.user_name).to eq("Mary Jean")
	end
end
