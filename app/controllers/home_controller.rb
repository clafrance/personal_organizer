class HomeController < ApplicationController
	
  def index
  	if user_signed_in?
  		@public_tasks = Task.public_tasks.order(created_at: :desc)
  		@private_tasks = Task.private_tasks.where(user_id: current_user.id).order(created_at: :desc)
    end
  end
end
