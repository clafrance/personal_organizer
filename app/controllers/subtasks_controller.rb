class SubtasksController < ApplicationController
	before_action :authenticate_user!
	before_action :set_subtask, only: [:edit, :update, :destroy]

	respond_to :html
	respond_to :js

	def new
		@task = Task.find(params[:task_id])
		@subtask = @task.subtasks.build
		respond_with(@subtasks)
	end

	def create
		task = Task.find(params[:task_id])
		@subtask = task.subtasks.build(subtask_params)

		@subtask.save
		respond_with(@subtask)
	end

	def edit

	end

	def update
		@subtask.update(subtask_params)
		respond_with(@subtask)
	end

  def destroy
    @subtask.destroy
    respond_with(@subtask)
  end

	private
    def set_subtask
      @subtask = Subtask.find(params[:id])
    end

    def subtask_params
      params.require(:subtask).permit(:body, :task_id)
    end
end