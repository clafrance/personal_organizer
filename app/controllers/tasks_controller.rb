class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_task, only: [:edit, :update, :destroy, :mark_complete]

  def index
    @my_public_tasks = Task.public_tasks.where(user_id: current_user.id).order(created_at: :desc)
    @my_private_tasks = Task.private_tasks.where(user_id: current_user.id).order(created_at: :desc)
  end

  def show
    @task = Task.includes(:subtasks).find(params[:id])
    @is_private = @task.is_private ? "Private" : "Public"
    @subtasks = @task.subtasks
  end

  def new
    @task = current_user.tasks.build
  end

  def edit
  end

  def create
    @task = current_user.tasks.build(task_params)
    if @task.save
      flash.now[:success] = "task was successfully created."
      redirect_to tasks_url
    else
      render :new
    end
  end

  def update
    if item_owner?(current_user, @task)
      if @task.update(task_params)
        flash[:success] = "task was successfully updated."
        redirect_to @task
      else
        render :edit
      end
    else
      flash[:warning] = "You can only edit your own tasks."
      redirect_to @task
    end
  end

  def destroy
    if item_owner?(current_user, @task)
      @task.destroy
      flash.now[:success] = "task was successfully deleted."
      redirect_to tasks_url
    else
      flash[:warning] = "You can only delete your own tasks."
      redirect_to @task
    end
  end

  def mark_complete
    @task.update_attributes(completed: true)
    redirect_to @task
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:body, :is_private)
    end
end
