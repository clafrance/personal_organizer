module TasksHelper
	def item_owner?(user, item)
		user == item.user
	end
end
