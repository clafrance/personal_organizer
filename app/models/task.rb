class Task < ApplicationRecord 
  VALID_NAME_REGEX = /\A([a-z]|[A-Z]|[0-9]|[!*&@!#%$^+-.,\'\" ])+\z/

  validates :body, presence: true,
            length: { maximum: 128 },
            format: { with: VALID_NAME_REGEX }

  belongs_to :user
  has_many   :subtasks, dependent: :destroy

  scope :public_tasks,  -> { includes(:subtasks).where(is_private: false) }
  scope :private_tasks, -> { includes(:subtasks).where(is_private: true) }

  def private
  	self.is_private ? "Private" : "Public"
  end

  def user_name
  	self.is_private ? "" : self.user.first_name + " " + self.user.last_name
  end

  def status
    self.completed ? "Completed: Yes" : "Completed: No"
  end

  def display_completed
    "(Completed)"
  end
end

