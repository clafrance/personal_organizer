class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  VALID_NAME_REGEX = /\A([a-z]|[A-Z]|[0-9]|[-._])+\z/

  validates :first_name, presence: true,
            length: { maximum: 64 },
            format: { with: VALID_NAME_REGEX }

  validates :last_name, presence: true,
            length: { maximum: 64 },
            format: { with: VALID_NAME_REGEX }

  has_many :tasks, dependent: :destroy

  def full_name
    self.first_name + " " + self.last_name
  end

  def greeting
    "Hello " + self.full_name + ", " + self.email
  end
end
