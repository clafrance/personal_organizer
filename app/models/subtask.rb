class Subtask < ApplicationRecord
  VALID_NAME_REGEX = /\A([a-z]|[A-Z]|[0-9]|[!*&@!#%$^+-.,\'\" ])+\z/

  validates :body, presence: true,
            length: { maximum: 128 },
            format: { with: VALID_NAME_REGEX }

  belongs_to :task
end
