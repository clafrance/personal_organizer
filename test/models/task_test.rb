require 'test_helper'

class TaskTest < ActiveSupport::TestCase

	task1 = Task.new(body: "thisisatesttask", is_private: true, user_id: 1)
	task2 = Task.new(body: "thisisatesttask", is_private: false, user_id: 1)

	test "private mathod should return private"  do
  	assert_equal "Private", task1.private	
  end

  test "private mathod should return public"  do
  	assert_equal "Public", task2.private	
  end

  # test "user_name mathod should empty string"  do
  # 	assert_equal "", task1.user_name	
  # end

  # test "private mathod should return public"  do
  # 	assert_equal "", task2.user_name	
  # end

end
