require 'test_helper'

class UserTest < ActiveSupport::TestCase

	it "returns full_name as Mary Jean" do
		user = build(:user)
		expect(user.full_name).to eq("Mary Jean")
	end

	it "returns greeting as Mary Jean" do
		user = build(:user)
		expect(user.greeting).to eq("Hello Mary Jean, email@gmail.com")
	end
end
