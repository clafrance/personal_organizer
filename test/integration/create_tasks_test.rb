require "application_system_test_case.rb"

class CreateTasksTest < ApplicationSystemTestCase

  test "visiting the index" do
    visit(root_url)
    click_link('Sign Up')
    fill_in('Email', with: 'abc@email.com')
    fill_in('First name', with: 'christie')
    fill_in('Last name', with: 'lafrance')
    fill_in('Password', with: 'password')
    fill_in('Password confirmation', with: 'password')
    click_button('Sign up')
    page.has_content?('Todo List')
    click_link('Create New Task') 
    page.has_content?('New Task')
    fill_in('Body', with: 'Task1')
    click_button('Create Task')
    page.has_content?('My Tasks')
    page.has_content?('Task1')
    click_link('Task1')
    page.has_content?('View Task Details')
    page.has_content?('Public Task, Task1')
    click_link('Create New Subtask')
    page.has_content?('Create Subtask')
    fill_in('Body', with: 'Subtask1')
    click_button('Create Subtask')
    page.has_content?('Subtask1')
    page.has_content?('Create New Subtask')
  end
end
